from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from conexao import _func

# http://data.phishtank.com/data/online-valid.json
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

class Domains(db.Model):
    __tablename__= 'MyDomains'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    link = db.Column(db.String(300), unique=True)

    def __init__(self, id=None, link=None):
        self.id = id
        self.link = link

    def __repr__(self):
        return '{}'.format(self.link)

db.create_all()


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/_searching', methods=['POST'])
def _searching():
    req = request.form['domain']
    w = _func(req)
    for links in w:
        db.session.add(Domains(link=links))

    db.session.commit()
    return render_template('index.html', urls=Domains.query.all())


if __name__ == '__main__':
   app.run(debug = True, host='0.0.0.0')
