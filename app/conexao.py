import requests
import ast #ast.literal_eval

def _func(my_url):
    r = ast.literal_eval(requests.get(my_url).text)
    lista = []
    for elemento in r:
        lista.append(elemento['url'])
    return set(lista)
